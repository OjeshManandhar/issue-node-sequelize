import path from 'path';

// packages
import { DataTypes, Sequelize } from 'sequelize';

// env
import { SHOW_DB_LOGS, LOAD_DB_FROM_MEMORY } from 'env_config';

const dbPath = path.join(__dirname, 'issues.db');

const sequelize = new Sequelize('', '', '', {
  dialect: 'sqlite',
  storage: LOAD_DB_FROM_MEMORY ? ':memory:' : dbPath,
  logging: SHOW_DB_LOGS
});

// Test connection
// (async () => {
//   try {
//     await sequelize.authenticate();
//     console.log('Connection has been established successfully.');
//   } catch (error) {
//     console.error('Unable to connect to the database:', error);
//   }
// })();

export { DataTypes };
export default sequelize;
