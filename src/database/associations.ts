// models
import Role from 'models/role';
import User from 'models/user';
import Issue from 'models/issue';
import Member from 'models/member';
import Project from 'models/project';

export default (): void => {
  // Role - User => one -to-many
  Role.hasMany(User, {
    foreignKey: {
      name: 'role_id'
    }
  });
  User.belongsTo(Role, {
    foreignKey: {
      name: 'role_id'
    }
  });

  // User(creator) - Project => one-to-many
  User.hasMany(Project, {
    foreignKey: {
      name: 'creator_id'
    },
    as: 'creatorOf'
  });
  Project.belongsTo(User, {
    foreignKey: {
      name: 'creator_id'
    },
    as: 'creator'
  });

  // User(members) - Project => many-to-many through Member
  User.belongsToMany(Project, {
    through: Member,
    foreignKey: 'user_id', // replaces `userId`
    otherKey: 'project_id', // replaces `projectId`
    as: 'memberOf'
  });
  Project.belongsToMany(User, {
    through: Member,
    otherKey: 'user_id', // replaces `userId`
    foreignKey: 'project_id', // replaces `projectId`
    as: 'members'
  });

  // Project - Issue => one-to-many
  Project.hasMany(Issue, {
    foreignKey: {
      name: 'project_id'
    }
  });
  Issue.belongsTo(Project, {
    foreignKey: {
      name: 'project_id'
    }
  });

  // User(creator) - Issue => one-to-many
  User.hasMany(Issue, {
    foreignKey: {
      name: 'creator_id'
    },
    as: 'createdIssue'
  });
  Issue.belongsTo(User, {
    foreignKey: {
      name: 'creator_id'
    },
    as: 'creator'
  });

  // User(assignee) - Issue => one-to-many
  User.hasMany(Issue, {
    foreignKey: {
      name: 'assignee_id',
      allowNull: true
    },
    as: 'assignedTo'
  });
  Issue.belongsTo(User, {
    foreignKey: {
      name: 'assignee_id',
      allowNull: true
    },
    as: 'assignee'
  });
};
