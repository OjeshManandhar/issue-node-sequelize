// pcakages
import type { ValidationError } from 'express-validator';

type _Error = {
  msg: string;
  param?: string;
  value?: string;
  location?: string;
};

export type ErrorData = Array<_Error | ValidationError>;
