// types
import { ErrorData } from './types';

class CustomError extends Error {
  status: number;
  data: ErrorData;

  constructor(
    message = 'An error occured',
    status = 500,
    data: ErrorData = []
  ) {
    super(message);

    // For instanceof to work
    Object.setPrototypeOf(this, CustomError.prototype);

    this.status = status;
    this.data = data;
  }
}

export type { ErrorData };
export default CustomError;
