// global
import type { Token } from 'global/types';

export type EncodeIdToJwt = (id: string, exp?: string | number) => string;

export type VerifyToken = (token: string) => Promise<Token>;
