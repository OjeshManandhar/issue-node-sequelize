// packages
import jwt from 'jsonwebtoken';

// env
import { JWT_SECRET } from 'env_config';

// utils
import CustomError from 'utils/customError';

// types
import type { Token } from 'global/types';
import type { VerifyToken, EncodeIdToJwt } from './types';

export const encodeIdToJwt: EncodeIdToJwt = (id, exp) => {
  if (!JWT_SECRET) {
    throw new CustomError('JWT error');
  }

  const tokenObj: Token = { id };

  const token = jwt.sign(tokenObj, JWT_SECRET, {
    expiresIn: exp || '1d'
  });

  return token;
};

export const verifyToken: VerifyToken = token =>
  new Promise((resolve, reject) => {
    if (!JWT_SECRET) {
      reject(new CustomError('JWT error'));

      return;
    }

    jwt.verify(token, JWT_SECRET, (err, decoded) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(decoded as Token);
    });
  });
