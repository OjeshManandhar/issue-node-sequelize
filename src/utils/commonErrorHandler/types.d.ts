export type CommonErrorHandler = (err: Error, msg: string) => Error;
