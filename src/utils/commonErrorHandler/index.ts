// utils
import CustomError from 'utils/customError';

// env
import { DEV_FEATURE } from 'env_config';

// types
import { CommonErrorHandler } from './types';

const commonErrorHandler: CommonErrorHandler = (err, msg) => {
  if (err instanceof CustomError) {
    return err;
  } else {
    if (process.env.NODE_ENV !== 'test') console.log('Error:', err);

    return new CustomError(
      msg,
      500,
      DEV_FEATURE ? [{ msg: err.message }] : undefined
    );
  }
};

export default commonErrorHandler;
