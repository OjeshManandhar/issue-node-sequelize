// packages
import { TokenExpiredError } from 'jsonwebtoken';

// utils
import { verifyToken } from 'utils/token';
import CustomError from 'utils/customError';

// global
import type { Token } from 'global/types';

// types
import type { Request, Response, NextFunction } from 'express';

export default (req: Request, res: Response, next: NextFunction): void => {
  const authHeader = req.headers?.authorization;

  req.userId = undefined;

  if (!authHeader) {
    return next(new CustomError('No authorization header', 422));
  }

  const token = authHeader.split(' ')[1];
  if (!token) {
    return next(new CustomError('Invalid authorization header', 422));
  }

  verifyToken(token)
    .then((token: Token) => {
      if (token.id) {
        req.userId = parseInt(token.id, 10);

        next();
      } else {
        next(
          new CustomError('Invalid token', 422, [
            {
              msg: 'Token has been changed',
              param: 'authorization',
              location: 'header'
            }
          ])
        );
      }
    })
    .catch(err => {
      const expired = err instanceof TokenExpiredError;

      next(
        new CustomError('Invalid token', 422, [
          {
            msg: expired ? 'Token expired' : 'Invalid Signature',
            param: 'authorization',
            location: 'header'
          }
        ])
      );
    });
};
