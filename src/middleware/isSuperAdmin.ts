// model
import User from 'models/user';

// utils
import CustomError from 'utils/customError';
import commonErrorHandler from 'utils/commonErrorHandler';

// global
import { Roles } from 'global/const';

// types
import type { Request, Response, NextFunction } from 'express';

const isSuperAdmin: (req: Request, res: Response, next: NextFunction) => void =
  async (req, res, next) => {
    try {
      // Find logged in user
      const curUser = await User.findByPk(req.userId);

      if (!curUser) {
        throw new CustomError('User not found', 401);
      }
      // Check for SuperAdmin
      if (curUser.role_id !== Roles.SuperAdmin.id) {
        throw new CustomError('Unauthorized', 401);
      }

      next();
    } catch (err) {
      next(commonErrorHandler(err, 'Something went wrong'));
    }
  };

export default isSuperAdmin;
