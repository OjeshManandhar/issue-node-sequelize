// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

// packages
import bodyParser from 'body-parser';
import express, { Request, Response, NextFunction } from 'express';

// database
import sequelize from 'database';
import setAssociations from 'database/associations';

// model
import Role from 'models/role';

// router
import RoleRoutes from 'routes/role';
import UserRoutes from 'routes/user';
import IssueRoutes from 'routes/issue';
import ProjectRoutes from 'routes/project';

// utils
import CustomError from 'utils/customError';

// global
import { Roles } from 'global/const';

// env
import { PORT, FORCE_SYNC_DB, LOAD_DB_FROM_MEMORY } from 'env_config';

const app = express();

// Request parser
app.use(bodyParser.json());

// CORS headers
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

// Routes
app.use('/role', RoleRoutes);
app.use('/user', UserRoutes);
app.use('/issue', IssueRoutes);
app.use('/project', ProjectRoutes);

// 404
app.use('/', (req, res) => {
  res.status(404).json({ message: 'URL Not found' });
});

// Error handler
app.use(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (error: CustomError, req: Request, res: Response, next: NextFunction) => {
    const status = error.status || 500;
    const message = error.message || 'An error occured';
    const data = error.data || [];

    res.status(status).json({ message: message, status: status, data: data });
  }
);

setAssociations();

sequelize
  .sync(FORCE_SYNC_DB ? { force: true } : undefined)
  .then(() =>
    FORCE_SYNC_DB || LOAD_DB_FROM_MEMORY
      ? Role.bulkCreate(Object.values(Roles))
      : undefined
  )
  .then(() => {
    app.listen(PORT, () => console.log('Server started at port:', PORT));
  })
  .catch(err => console.log('Could not sync DB:', err));
