// packages
import type { Model, Optional } from 'sequelize';

interface ProjectAttributes {
  id: number;
  name: string;
  description: string;
  creator_id: number;
}

// Some fields are optional when calling ProjectModel.create() or ProjectModel.build()
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ProjectCreationAttributes extends Optional<ProjectAttributes, 'id'> {}

// We need to declare an interface for our model that is basically what our class would be
export interface ProjectInstance
  extends Model<ProjectAttributes, ProjectCreationAttributes>,
    ProjectAttributes {}
