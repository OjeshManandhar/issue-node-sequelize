// database
import sequelize, { DataTypes } from 'database';

// types
import { ProjectInstance } from './types';

const Project = sequelize.define<ProjectInstance>('project', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: DataTypes.TEXT,
    allowNull: false,
    unique: true
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: false
  },
  creator_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  }
});

export default Project;
export type { ProjectInstance };
