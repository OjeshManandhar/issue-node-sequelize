// packages
import type { Model, Optional } from 'sequelize';

interface MemberAttributes {
  user_id: number;
  project_id: number;
}

// Some fields are optional when calling MemberModel.create() or MemberModel.build()
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface MemberCreationAttributes extends Optional<MemberAttributes, 'id'> {}

// We need to declare an interface for our model that is basically what our class would be
export interface MemberInstance
  extends Model<MemberAttributes, MemberCreationAttributes>,
    MemberAttributes {}
