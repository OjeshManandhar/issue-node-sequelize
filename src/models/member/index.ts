// database
import sequelize, { DataTypes } from 'database';

// model
import User from 'models/user';
import Project from 'models/project';

// types
import { MemberInstance } from './types';

const Member = sequelize.define<MemberInstance>('member', {
  user_id: {
    type: DataTypes.INTEGER,
    references: {
      model: User,
      key: 'id'
    }
  },
  project_id: {
    type: DataTypes.INTEGER,
    references: {
      model: Project,
      key: 'id'
    }
  }
});

export default Member;
export type { MemberInstance };
