// packages
import type { Model, Optional } from 'sequelize';

// global
import { ISSUE_STATUS } from 'global/enum';

interface IssueAttributes {
  id: number;
  project_id: number;
  creator_id: number;
  title: string;
  description: string;
  status: ISSUE_STATUS;
  assignee_id: number;
  created_at: Date;
}

// Some fields are optional when calling IssueModel.create() or IssueModel.build()
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IssueCreationAttributes extends Optional<IssueAttributes, 'id'> {}

// We need to declare an interface for our model that is basically what our class would be
export interface IssueInstance
  extends Model<IssueAttributes, IssueCreationAttributes>,
    IssueAttributes {}
