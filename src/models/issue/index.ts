// database
import sequelize, { DataTypes } from 'database';

// global
import { ISSUE_STATUS } from 'global/enum';

// types
import { IssueInstance } from './types';

const Issue = sequelize.define<IssueInstance>(
  'issue',
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    project_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: 'unique-project_id-title'
    },
    creator_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    title: {
      type: DataTypes.TEXT,
      allowNull: false,
      unique: 'unique-project_id-title'
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM(
        ISSUE_STATUS.OPEN,
        ISSUE_STATUS.ASSIGNED,
        ISSUE_STATUS.CLOSED
      )
    },
    assignee_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    created_at: DataTypes.NOW
  },
  {
    createdAt: 'created_at'
  }
);

export default Issue;
export type { IssueInstance };
