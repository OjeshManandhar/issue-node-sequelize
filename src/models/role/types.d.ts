// packages
import type { Model } from 'sequelize';

// We need to declare an interface for our model that is basically what our class would be
export interface RoleInstance extends Model {
  id: number;
  title: string;
}
