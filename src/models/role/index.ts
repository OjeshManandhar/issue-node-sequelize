// database
import sequelize, { DataTypes } from 'database';

// types
import type { RoleInstance } from './types';

const Role = sequelize.define<RoleInstance>('role', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  }
});

export default Role;
export type { RoleInstance };
