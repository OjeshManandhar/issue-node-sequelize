// packages
import type { Model, Optional } from 'sequelize';

interface UserAttributes {
  id: number;
  name: string;
  password: string;
  description: string;
  role_id: number;
}

// Some fields are optional when calling UserModel.create() or UserModel.build()
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface UserCreationAttributes extends Optional<UserAttributes, 'id'> {}

// We need to declare an interface for our model that is basically what our class would be
export interface UserInstance
  extends Model<UserAttributes, UserCreationAttributes>,
    UserAttributes {}
