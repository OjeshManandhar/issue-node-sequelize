// database
import sequelize, { DataTypes } from 'database';

// types
import type { UserInstance } from './types';

const User = sequelize.define<UserInstance>('user', {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: DataTypes.TEXT,
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.TEXT,
    allowNull: false,
    unique: false
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true
  },
  role_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  }
});

export default User;
export type { UserInstance };
