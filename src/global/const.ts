export const Roles = {
  SuperAdmin: {
    id: 1,
    title: 'SuperAdmin'
  },
  Manager: {
    id: 2,
    title: 'Manager'
  }
};
