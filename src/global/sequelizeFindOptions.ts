// packages
import cloneDeep from 'lodash.clonedeep';

// model
import Role from 'models/role';
import User from 'models/user';
import Project from 'models/project';

export const ATTRIBUTES = {
  role: ['id', 'title'],
  user: ['id', 'name', 'description'],
  project: ['id', 'name', 'description'],
  issue: ['id', 'title', 'description', 'status', 'created_at']
};

export const UserRole = {
  model: Role,
  as: 'role',
  attributes: ATTRIBUTES.role
};

export const ProjectCreator = {
  model: User,
  as: 'creator',
  attributes: ATTRIBUTES.user,
  include: [cloneDeep(UserRole)]
};

export const ProjectMembers = {
  model: User,
  as: 'members',
  attributes: ATTRIBUTES.user,
  include: [cloneDeep(UserRole)],
  through: { attributes: [] }
};

export const UserCreatorOf = {
  model: Project,
  as: 'creatorOf',
  attributes: ATTRIBUTES.project,
  include: [cloneDeep(ProjectMembers)]
};

export const UserMemberOf = {
  model: Project,
  as: 'memberOf',
  attributes: ATTRIBUTES.project,
  include: [cloneDeep(ProjectCreator), cloneDeep(ProjectMembers)],
  through: { attributes: [] }
};

export const IssueCreator = {
  model: User,
  as: 'creator',
  attributes: ATTRIBUTES.user,
  include: [cloneDeep(UserRole)]
};

export const IssueAssignee = {
  model: User,
  as: 'assignee',
  attributes: ATTRIBUTES.user,
  include: [cloneDeep(UserRole)]
};

export const IssueProject = {
  model: Project,
  as: 'project',
  attributes: ATTRIBUTES.project,
  include: [cloneDeep(ProjectCreator), cloneDeep(ProjectMembers)]
};
