// package
import { validationResult } from 'express-validator';

// model
import User from 'models/user';
import Project from 'models/project';
import Role, { RoleInstance } from 'models/role';

// utils
import CustomError from 'utils/customError';
import commonErrorHandler from 'utils/commonErrorHandler';

// global
import { Roles } from 'global/const';
import {
  ATTRIBUTES,
  ProjectCreator,
  ProjectMembers
} from 'global/sequelizeFindOptions';

// types
import type { Request, Response, NextFunction } from 'express';

const INCLUDE = [ProjectCreator, ProjectMembers];

export const createProject: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const { name, description } = req.body;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId, { include: Role });

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }
    // Check for SuperAdmin or Manager
    if (
      curUser.role_id !== Roles.SuperAdmin.id &&
      curUser.role_id !== Roles.Manager.id
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const role: RoleInstance = (curUser as any).role;

    // Create project
    const project = await Project.create({
      name,
      description,
      creator_id: curUser.id
    });

    return res.json({
      name: project.name,
      description: project.description,
      creator: {
        id: curUser.id,
        name: curUser.name,
        description: curUser.description,
        role: {
          id: role.id,
          title: role.title
        }
      }
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const listProjects: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }

    // Fetch all projects
    const projects = await Project.findAll({
      attributes: ATTRIBUTES.project,
      include: INCLUDE
    });

    res.json(projects.map(p => p.toJSON()));
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const deleteProject: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const id = parseInt(req.params.id, 10);

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }
    // Check for SuperAdmin or Manager
    if (
      curUser.role_id !== Roles.SuperAdmin.id &&
      curUser.role_id !== Roles.Manager.id
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    // Find Project
    const project = await Project.findByPk(id, {
      attributes: [...ATTRIBUTES.project, 'creator_id'],
      include: INCLUDE
    });

    if (!project) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: id,
          msg: 'Project not found',
          param: 'id',
          location: 'params'
        }
      ]);
    }

    // Check if curUser is the creator or SuperAdmin
    if (
      req.userId !== Roles.SuperAdmin.id &&
      project.creator_id !== req.userId
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    await project.destroy();

    const _project = project.toJSON();
    // Remove creator_id
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    delete (_project as any)['creator_id'];

    res.json(_project);
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const addMember: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const { user_id, project_id } = req.body;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }
    // Check for SuperAdmin or Manager
    if (
      curUser.role_id !== Roles.SuperAdmin.id &&
      curUser.role_id !== Roles.Manager.id
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    // Find user to add
    const user = await User.findByPk(user_id);

    if (!user) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: user_id,
          msg: 'User not found',
          param: 'user_id',
          location: 'params'
        }
      ]);
    }

    // Find project
    const project = await Project.findByPk(project_id, {
      include: [ProjectMembers]
    });

    if (!project) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: project_id,
          msg: 'Project not found',
          param: 'project_id',
          location: 'params'
        }
      ]);
    }

    // Check if curUser has access to project or not
    if (
      curUser.id !== project.creator_id &&
      curUser.role_id !== Roles.SuperAdmin.id
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    // Check if user to add is SuperAdmin or Creator
    if (
      user.role_id === Roles.SuperAdmin.id ||
      user.id === project.creator_id
    ) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: user_id,
          msg: 'SuperAdmin and Creator cannot be added as Member',
          param: 'user_id',
          location: 'params'
        }
      ]);
    }

    // Check if user is already added
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (project.members.find(mem => mem.id === user.id)) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: user_id,
          msg: 'User already added as Member',
          param: 'user_id',
          location: 'params'
        }
      ]);
    }

    // Add using association
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    await project.addMember(user);

    /**
     * The above method does not return updated project
     * and project will not have the newly added member
     * so re-fetch the product from DB
     */
    const _proj = await Project.findByPk(project.id, {
      attributes: ATTRIBUTES.project,
      include: INCLUDE
    });

    if (!_proj) {
      throw new CustomError('Fetching updated project failed');
    }

    res.json(_proj.toJSON());
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const removeMember: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const { user_id, project_id } = req.body;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }
    // Check for SuperAdmin or Manager
    if (
      curUser.role_id !== Roles.SuperAdmin.id &&
      curUser.role_id !== Roles.Manager.id
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    // Find user to remove
    const user = await User.findByPk(user_id);

    if (!user) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: user_id,
          msg: 'User not found',
          param: 'user_id',
          location: 'params'
        }
      ]);
    }

    // Find project
    const project = await Project.findByPk(project_id, {
      include: [ProjectMembers]
    });

    if (!project) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: project_id,
          msg: 'Project not found',
          param: 'project_id',
          location: 'params'
        }
      ]);
    }

    // Check if curUser has access to project or not
    if (
      curUser.id !== project.creator_id &&
      curUser.role_id !== Roles.SuperAdmin.id
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    // Check if user to remove is SuperAdmin or Creator
    if (
      user.role_id === Roles.SuperAdmin.id ||
      user.id === project.creator_id
    ) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: user_id,
          msg: 'SuperAdmin and Creator cannot be removed as Member',
          param: 'user_id',
          location: 'params'
        }
      ]);
    }

    // Check if user is present or not
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (!project.members.find(mem => mem.id === user.id)) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: user_id,
          msg: 'User is not a Member of the project',
          param: 'user_id',
          location: 'params'
        }
      ]);
    }

    // Remove using association
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    await project.removeMember(user);

    /**
     * The above method does not return updated project
     * and project will not have the newly added member
     * so re-fetch the product from DB
     */
    const _proj = await Project.findByPk(project.id, {
      attributes: ATTRIBUTES.project,
      include: INCLUDE
    });

    if (!_proj) {
      throw new CustomError('Fetching updated project failed');
    }

    res.json(_proj.toJSON());
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};
