// packages
import { hash } from 'bcryptjs';
import { compare } from 'bcryptjs';
import { validationResult } from 'express-validator';

// model
import Role from 'models/role';
import User from 'models/user';

// utils
import { encodeIdToJwt } from 'utils/token';
import commonErrorHandler from 'utils/commonErrorHandler';
import CustomError, { ErrorData } from 'utils/customError';

// global
import { Roles } from 'global/const';
import {
  ATTRIBUTES,
  UserRole,
  UserMemberOf,
  UserCreatorOf
} from 'global/sequelizeFindOptions';

// types
import type { Request, Response, NextFunction } from 'express';

const INCLUDE = [UserRole, UserCreatorOf, UserMemberOf];

export const createSuperAdmin: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const { name, password, description } = req.body;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Check if other users exist
    const users = await User.findAll();

    if (users.length > 0) {
      throw new CustomError('Unauthorized', 401);
    }

    // Create new user
    const hashedPass = await hash(password, 12);

    const superAdmin = await User.create({
      name,
      password: hashedPass,
      description,
      role_id: Roles.SuperAdmin.id
    });

    // Create JWT
    const token = encodeIdToJwt(superAdmin.id.toString(), '30m');

    res.json({
      token,
      id: superAdmin.id,
      name: superAdmin.name,
      description: superAdmin.description,
      role: Roles.SuperAdmin
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const createUser: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const { name, password, description, role_id } = req.body;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }
    // Check for SuperAdmin
    if (curUser.role_id !== Roles.SuperAdmin.id) {
      throw new CustomError('Unauthorized', 401);
    }

    // Check if user already exists
    const existingUser = await User.findOne({
      where: { name }
    });

    if (existingUser) {
      throw new CustomError('User already exists', 401, [
        {
          msg: 'name already in use',
          param: 'name',
          location: 'body'
        }
      ]);
    }

    // Check the role of new user
    const role = await Role.findByPk(role_id);

    if (!role) {
      throw new CustomError('Invalid Input', 422, [
        { msg: 'Invalid role_id', param: 'role_id', location: 'body' }
      ]);
    }

    // Create new user
    const hashedPass = await hash(password, 12);

    const newUser = await User.create({
      name,
      password: hashedPass,
      description,
      role_id
    });

    // Make JWT
    const token = encodeIdToJwt(newUser.id.toString());

    res.json({
      token,
      id: newUser.id,
      name: newUser.name,
      description: newUser.description,
      role: { id: role.id, title: role.title }
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const login: (req: Request, res: Response, next: NextFunction) => void =
  async (req, res, next) => {
    const encodedAuth = req.headers?.authorization;

    if (!encodedAuth) {
      return next(
        new CustomError('Invalid Input', 422, [
          {
            msg: 'No authorization header',
            param: 'authorization',
            location: 'header'
          }
        ])
      );
    }

    const decodedAuth = Buffer.from(
      encodedAuth.split(' ')[1],
      'base64'
    ).toString();

    const auth = {
      name: decodedAuth.split(':')[0].toString().trim(),
      password: decodedAuth.split(':')[1].toString().trim()
    };

    if (!auth.name || !auth.password) {
      return next(
        new CustomError('Invalid Input', 422, [
          {
            msg: 'Invalid authorization header',
            param: 'authorization',
            location: 'header'
          }
        ])
      );
    }

    // Validation
    const errors: ErrorData = [];

    if (auth.name.length < 4 || auth.name.length > 20) {
      errors.push({
        msg: 'name must have 4 to 20 characters',
        value: auth.name,
        location: 'header',
        param: 'authorization'
      });
    }
    if (auth.password.length < 8) {
      errors.push({
        msg: 'passowrd must be at least 15 characters',
        value: auth.name,
        location: 'header',
        param: 'authorization'
      });
    }

    if (errors.length > 0) {
      return next(new CustomError('Invalid Input', 422, errors));
    }

    try {
      // Find user
      const existingUser = await User.findOne({
        where: { name: auth.name },
        attributes: ['password', ...ATTRIBUTES.user],
        include: INCLUDE
      });

      if (!existingUser) {
        throw new CustomError('Incorrect username or password', 401);
      }

      // Compare password
      const passwordMatch = await compare(auth.password, existingUser.password);

      if (!passwordMatch) {
        throw new CustomError('Incorrect username or password', 401);
      }

      // Make JWT
      const token = encodeIdToJwt(
        existingUser.id.toString(),
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        existingUser.role.id === Roles.SuperAdmin.id ? '30m' : undefined
      );

      const userDataToSend = { ...existingUser.toJSON() };

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      delete userDataToSend['password'];

      res.json({
        token,
        ...userDataToSend
      });
    } catch (err) {
      next(commonErrorHandler(err, 'Something went wrong'));
    }
  };

export const listUsers: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }

    const users = await User.findAll({
      attributes: ATTRIBUTES.user,
      include: INCLUDE
    });

    res.send({ users: users.map(u => u.toJSON()) });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const deleteUser: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const id = parseInt(req.params.id, 10);

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }
    // Check for SuperAdmin
    if (curUser.role_id !== Roles.SuperAdmin.id) {
      throw new CustomError('Unauthorized', 401);
    }

    // Find user to delete
    const user = await User.findByPk(id, {
      attributes: ATTRIBUTES.user,
      include: INCLUDE
    });

    if (!user) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: id,
          msg: 'User not found',
          param: 'id',
          location: 'params'
        }
      ]);
    }

    // Check if user to delete is superAdmin
    if (user.role_id === Roles.SuperAdmin.id) {
      if (req.userId && id !== req.userId) {
        throw new CustomError(
          `Unauthorized. ${Roles.SuperAdmin.title} account can only be deleted by themselves.`
        );
      }
    }

    await user.destroy();

    res.send({ user: user.toJSON() });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};
