// packages
import { validationResult } from 'express-validator';

// model
import Role, { RoleInstance } from 'models/role';

// utils
import CustomError from 'utils/customError';
import commonErrorHandler from 'utils/commonErrorHandler';

// types
import type { Request, Response, NextFunction } from 'express';

export const createRole: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const { title } = req.body;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    const role = await Role.create({ title });

    res.json({
      id: role.id,
      title: role.title
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const listRoles: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  try {
    const roles = await Role.findAll();

    const formattedRole = roles.map(role => {
      const temp: Partial<RoleInstance> = role.toJSON();

      return {
        id: temp.id,
        title: temp.title
      };
    });

    res.json({
      roles: formattedRole
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const deleteRole: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const id = req.params.id;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    const role = await Role.findByPk(id);

    if (!role) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: id,
          msg: 'Role not found',
          param: 'id',
          location: 'params'
        }
      ]);
    }

    await role.destroy();

    res.json({
      id: role.id,
      title: role.title
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};
