// package
import { validationResult } from 'express-validator';

// model
import User from 'models/user';
import Issue from 'models/issue';
import Project from 'models/project';

// utils
import CustomError from 'utils/customError';
import commonErrorHandler from 'utils/commonErrorHandler';

// global
import { Roles } from 'global/const';
import { ISSUE_STATUS } from 'global/enum';
import {
  ATTRIBUTES,
  UserRole,
  ProjectCreator,
  ProjectMembers,
  IssueCreator,
  IssueAssignee,
  IssueProject
} from 'global/sequelizeFindOptions';

// types
import type { Request, Response, NextFunction } from 'express';

const INCLUDE = [IssueCreator, IssueAssignee];

export const createIssue: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  let status = ISSUE_STATUS.OPEN;
  const { project_id, title, description, assignee_id } = req.body;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId, {
      attributes: ATTRIBUTES.user,
      include: [UserRole]
    });

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }

    // Find project
    const project = await Project.findByPk(project_id, {
      attributes: ATTRIBUTES.project,
      include: [ProjectCreator, ProjectMembers]
    });

    if (!project) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: project_id,
          msg: 'Project not found',
          param: 'project_id',
          location: 'body'
        }
      ]);
    }

    // Check if curUser can create issue for the project or not
    if (
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      curUser.role.id !== Roles.SuperAdmin.id &&
      curUser.id !== project.creator_id &&
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      !(await project.hasMember(curUser))
    ) {
      throw new CustomError('Unauthorized', 401);
    }

    let assignee = null;

    if (assignee_id) {
      // Find assignee if assignee_id is given
      assignee = await User.findByPk(assignee_id, {
        attributes: ATTRIBUTES.user,
        include: [UserRole]
      });

      if (!assignee) {
        throw new CustomError('Invalid Input', 422, [
          {
            value: assignee_id,
            msg: 'Assignee not found',
            param: 'assignee_id',
            location: 'body'
          }
        ]);
      }

      // Check if assignee is a member of the project
      if (
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        assignee.role.id !== Roles.SuperAdmin.id &&
        assignee.id !== project.creator_id &&
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        !(await project.hasMember(assignee))
      ) {
        throw new CustomError('Not allowed', 401, [
          {
            value: assignee_id,
            msg: 'Assignee is not a member of the project',
            param: 'assignee_id',
            location: 'body'
          }
        ]);
      }

      status = ISSUE_STATUS.ASSIGNED;
    }

    const issue = await Issue.create({
      title,
      project_id,
      creator_id: curUser.id,
      description,
      status,
      assignee_id,
      created_at: new Date()
    });

    res.json({
      id: issue.id,
      title: issue.title,
      description: issue.description,
      status: issue.status,
      created_at: issue.created_at,
      creator: curUser,
      assignee,
      project
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const listIssues: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const project_id = parseInt(req.params.project_id, 10);

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }

    // Find project
    const project = await Project.findByPk(project_id, {
      attributes: ATTRIBUTES.project,
      include: [ProjectCreator, ProjectMembers]
    });

    if (!project) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: project_id,
          msg: 'Project not found',
          param: 'project_id',
          location: 'params'
        }
      ]);
    }

    // Find all issues of the project
    const issues = await Issue.findAll({
      where: { project_id },
      attributes: ATTRIBUTES.issue,
      include: INCLUDE
    });

    res.json({
      issues: issues.map(i => i.toJSON())
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const closeIssue: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const id = req.params.id;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }

    // Find issue
    const issue = await Issue.findByPk(id, {
      attributes: ATTRIBUTES.issue,
      include: [...INCLUDE, IssueProject]
    });

    if (!issue) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: id,
          msg: 'Issue not found',
          param: 'id',
          location: 'params'
        }
      ]);
    }

    // Check is issue is already closed
    if (issue.status === ISSUE_STATUS.CLOSED) {
      throw new CustomError('Issue already closed', 422);
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const project = issue.project;

    // Check if the curUser is member of the project
    if (
      curUser.role_id !== Roles.SuperAdmin.id &&
      curUser.id !== project.creator.id &&
      !(await project.hasMember(curUser))
    ) {
      throw new CustomError('Not allowed', 401, [
        {
          msg: 'You are not a member of the project'
        }
      ]);
    }

    // Close issue and update issue
    issue.status = ISSUE_STATUS.CLOSED;

    await issue.save();

    // Fetch updated issue
    const _issue = await Issue.findByPk(issue.id, {
      attributes: ATTRIBUTES.issue,
      include: INCLUDE
    });

    if (!_issue) {
      throw new CustomError('Error while fetching updated issue');
    }

    res.json({
      issue: _issue.toJSON()
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};

export const assignIssue: (
  req: Request,
  res: Response,
  next: NextFunction
) => void = async (req, res, next) => {
  const id = req.params.id;
  const assignee_id = req.body.assignee_id;

  // Validation
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return next(new CustomError('Ivalid Input', 422, errors.array()));
  }

  try {
    // Find logged in user
    const curUser = await User.findByPk(req.userId);

    if (!curUser) {
      throw new CustomError('User not found', 401);
    }

    // Find issue
    const issue = await Issue.findByPk(id, {
      attributes: ATTRIBUTES.issue,
      include: [...INCLUDE, IssueProject]
    });

    if (!issue) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: id,
          msg: 'Issue not found',
          param: 'id',
          location: 'params'
        }
      ]);
    }

    // Check is issue is already assigned
    if (issue.status === ISSUE_STATUS.ASSIGNED) {
      throw new CustomError('Issue already assigned', 422);
    }

    // Check is issue is already closed
    if (issue.status === ISSUE_STATUS.CLOSED) {
      throw new CustomError('Issue already closed', 422);
    }

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const project = issue.project;

    // Check if the curUser is member of the project
    if (
      curUser.role_id !== Roles.SuperAdmin.id &&
      curUser.id !== project.creator.id &&
      !(await project.hasMember(curUser))
    ) {
      throw new CustomError('Not allowed', 401, [
        {
          msg: 'You are not a member of the project'
        }
      ]);
    }

    // Check if assignee exists
    const assignee = await User.findByPk(assignee_id);

    if (!assignee) {
      throw new CustomError('Invalid Input', 422, [
        {
          value: assignee_id,
          msg: 'Assignee not found',
          param: 'assignee_id',
          location: 'body'
        }
      ]);
    }

    // Check if assignee is member of the project
    if (
      assignee.role_id !== Roles.SuperAdmin.id &&
      assignee.id !== project.creator.id &&
      !(await project.hasMember(assignee))
    ) {
      throw new CustomError('Not allowed', 401, [
        {
          value: assignee_id,
          msg: 'Assignee is not a member of the project',
          param: 'assignee_id',
          location: 'body'
        }
      ]);
    }

    // Assigne assignee to issue and update
    issue.status = ISSUE_STATUS.ASSIGNED;
    issue.assignee_id = assignee.id;

    await issue.save();

    // Fetch updated issue
    const _issue = await Issue.findByPk(issue.id, {
      attributes: ATTRIBUTES.issue,
      include: INCLUDE
    });

    if (!_issue) {
      throw new CustomError('Error while fetching updated issue');
    }

    res.json({
      issue: _issue.toJSON()
    });
  } catch (err) {
    next(commonErrorHandler(err, 'Something went wrong'));
  }
};
