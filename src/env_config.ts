export const PORT = process.env.PORT || 4000;

export const DEV_FEATURE =
  process.env.DEV_FEATURE?.toLowerCase() === 'true' ? true : false;

export const SHOW_DB_LOGS = process.env.SHOW_DB_LOGS
  ? process.env.SHOW_DB_LOGS.toLowerCase() === 'true'
  : true;

export const FORCE_SYNC_DB = process.env.FORCE_SYNC_DB
  ? process.env.FORCE_SYNC_DB.toLowerCase() === 'true'
  : true;

export const LOAD_DB_FROM_MEMORY = process.env.LOAD_DB_FROM_MEMORY
  ? process.env.LOAD_DB_FROM_MEMORY.toLowerCase() === 'true'
  : true;

export const JWT_SECRET = process.env.JWT_SECRET;
