// packages
import express from 'express';
import { body, param } from 'express-validator';

// controllers
import * as role from 'controllers/role';

// middleware
import isAuth from 'middleware/isAuth';
import isSuperAdmin from 'middleware/isSuperAdmin';

const router = express.Router();

router.use(isAuth);

router.post(
  '/create',
  isSuperAdmin,
  [
    body('title', 'is required and must be at least 3 characters')
      .trim()
      .isLength({ min: 3 })
  ],
  role.createRole
);

router.get('/list', role.listRoles);

router.delete(
  '/delete/:id',
  isSuperAdmin,
  [param('id', 'is required and must be a number').isNumeric().toInt()],
  role.deleteRole
);

export default router;
