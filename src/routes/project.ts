// packages
import express from 'express';
import { body, param } from 'express-validator';

// controllers
import * as project from 'controllers/project';

// middleware
import isAuth from 'middleware/isAuth';

const router = express.Router();

router.use(isAuth);

router.post(
  '/create',
  [
    body('name', 'is required and must be at least 5 characters')
      .trim()
      .isString()
      .isLength({ min: 5 }),
    body('description').trim()
  ],
  project.createProject
);

router.get('/list', project.listProjects);

router.delete(
  '/delete/:id',
  [param('id', 'is required and must be a number').isNumeric().toInt()],
  project.deleteProject
);

router.post(
  '/addMember',
  [
    body('user_id', 'is required and must be a number').isNumeric().toInt(),
    body('project_id', 'is required and must be a number').isNumeric().toInt()
  ],
  project.addMember
);

router.post(
  '/removeMember',
  [
    body('user_id', 'is required and must be a number').isNumeric().toInt(),
    body('project_id', 'is required and must be a number').isNumeric().toInt()
  ],
  project.removeMember
);

export default router;
