// packages
import express from 'express';
import { body, param } from 'express-validator';

// controllers
import * as issue from 'controllers/issue';

// middleware
import isAuth from 'middleware/isAuth';

const router = express.Router();

router.use(isAuth);

router.post(
  '/create',
  [
    body('project_id', 'is required and must be a number').isNumeric().toInt(),
    body('title', 'is required and must be at least 5 characters')
      .trim()
      .isString()
      .isLength({ min: 5 }),
    body('description').trim(),
    body('assignee_id', 'must be a number if given')
      .if(body('assignee_id').exists())
      .isNumeric()
      .toInt()
  ],
  issue.createIssue
);

router.get(
  '/list/:project_id',
  [param('project_id', 'is required and must be a number').isNumeric().toInt()],
  issue.listIssues
);

router.patch(
  '/close/:id',
  [param('id', 'is required and must be a number').isNumeric().toInt()],
  issue.closeIssue
);

router.patch(
  '/assign/:id',
  [
    param('id', 'is required and must be a number').isNumeric().toInt(),
    body('assignee_id', 'is required and must be a number').isNumeric().toInt()
  ],
  issue.assignIssue
);

export default router;
