// packages
import express from 'express';
import { body, param } from 'express-validator';

// controllers
import * as user from 'controllers/user';

// middleware
import isAuth from 'middleware/isAuth';

// global
import { Roles } from 'global/const';

const router = express.Router();

router.post(
  '/createSuperAdmin',
  [
    body('name', 'is required and must have 4 to 20 characters')
      .trim()
      .isString()
      .isLength({ min: 4, max: 20 }),
    body('password', 'is required and must be at least 15 characters')
      .trim()
      .isString()
      .isLength({ min: 15 }),
    body('description').trim()
  ],
  user.createSuperAdmin
);

router.post(
  '/create',
  isAuth,
  [
    body('name', 'is required and must have 4 to 20 characters')
      .trim()
      .isString()
      .isLength({ min: 4, max: 20 }),
    body('password', 'is required and must be at least 8 characters')
      .trim()
      .isString()
      .isLength({ min: 8 })
      .custom((value: string, { req }) => {
        if (req.body.role_id.toString() === Roles.SuperAdmin.id.toString()) {
          if (value.length < 15) {
            throw new Error(
              `must be at least 15 characters for ${Roles.SuperAdmin.title}`
            );
          }
        }

        return true;
      }),
    body('description').trim(),
    body('role_id', 'is required and must be a number').isNumeric().toInt()
  ],
  user.createUser
);

router.get('/login', user.login);

router.get('/list', isAuth, user.listUsers);

router.delete(
  '/delete/:id',
  isAuth,
  [param('id', 'is required and must be a number').isNumeric().toInt()],
  user.deleteUser
);

export default router;
